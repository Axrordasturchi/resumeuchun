<?php
   session_start();
    ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>dars2</title>
	<link rel="stylesheet" type="text/css" href="sinov.css">
    <link rel="stylesheet" type="text/css" href="sinovmo.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    
</head>
<body>
	
	<div id="sayt">
		<div class='tepa row'>
  			<div class="col-9 text-center">
                  <a class='sayt-nomi' href="bosh.html">Bilimdonlar<span class='uz'>.uz</span>
              </a>
            </div>
              <div class="btn btn-info col-2 " style="margin:10px ;">
   <?php echo $_SESSION['name'];?>
   <?php echo $_SESSION['familiya'];?>
	</div>
		</div>
		<div class="fan">
			<a class="fanlar html" href=".html">HTML</a>
			<a class="fanlar css" href=".html">CSS</a>
			<a class="fanlar js" href=".html">JS</a>
			<a class="fanlar php" href=".html">PHP</a>
			<a class="fanlar c" href=".html">C++</a>
			<a class="fanlar pyton" href=".html">Pyton</a>
			<a class="fanlar java" href=".html">JAVA</a>
		</div>
		<div class="dars">
			<ul>
			<li ><a class="mavzular" href="kirish.html">C++ ga kirish</a></li>
			<li><a class="mavzular" href=".htm">C++ O'zgaruvchilar</a></li>
			<li><a class="mavzular" href=".htm">C++ Chiqarish</a></li>
			<li><a class="mavzular" href=".htm">C++ Kiritish</a></li>
			<li><a class="mavzular" href=".htm">C++ Mantiqiy amallar</a></li>
			<li><a class="mavzular" href=".htm">C++ SHart operatori(if)</a></li>
			<li><a class="mavzular" href=".htm">C++ wheli</a></li>
			<li><a class="mavzular" href=".htm">C++ do wheli</a></li>
			<li><a class="mavzular" href=".htm" >C++ for</a></li>
			<li><a class="mavzular" href=".htm">C++ goto</a></li>
			<li><a class="mavzular" href=".htm">C++ switch</a></li>
			<li><a class="mavzular" href=".htm">C++ Break va continue operatorlari</a></li>
			<li><a class="mavzular" href=".htm">C++ Massivga kirish</a></li>
			<li><a class="mavzular" href=".htm">C++ 2 olchamli massiv</a></li>
			<li><a class="mavzular" href=".htm">C++ Ko'p o'lchamli massiv</a></li>
			<li><a class="mavzular" href=".htm">C++ So'zlar bilan ishlash</a></li>
			<li><a class="mavzular" href=".htm">C++ Funksiya tishunchasi</a></li>
			<li><a class="mavzular" href=".htm">C++ Funksiyalar</a></li>
			<li><a class="mavzular" href=".htm">C++ Rekursiv funksiyalar</a></li>
			<li><a class="mavzular" href=".htm">C++ Classlar tushunchasi</a></li>
			<li><a class="mavzular" href=".htm">C++ Classlar</a></li>
			<li><a class="mavzular" href=".htm">C++ Obektlar bilan ishlash</a></li>
			<li><a class="mavzular" href=".htm">C++ Fayllar bilan ishlash</a></li>
			</ul>
		</div>
		<div class="asos">
			<i>><p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		
		</p></i>
		</div>
		<div class="clear"></div>
	</div>
</body>
</html>