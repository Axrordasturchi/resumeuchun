<?php
require_once "include/db.php";

if (isset($_GET['id'])) {
	$id = (int)$_GET['id'];
}

$sql1 = "SELECT * FROM category WHERE id='$id'";
$res1 = mysqli_query($db, $sql1) or die("Bo'limni  tanlashda xatolik!");
$row1 = mysqli_fetch_assoc($res1);

if (mysqli_num_rows($res1) == 0) {
	header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="uz-UZ">
<head>
	<meta charset="<?=$row1['charset'];?>">
	<title><?=$row1['title'];?></title>
	<meta name="Description" content="<?=$row1['meta_d'];?>">
	<meta name="Keywords" content="<?=$row1['meta_k'];?>">

	<link rel="stylesheet" href="bootstrap.css">
	<link rel="stylesheet" href="styleblok.css" >
</head>

<body>
	<table border="1" width="1000" align="center">
		<?php include "include/head.php";?>
		
		<tr><td>
			<table border="1" width="100%" align="center" height="500">
				<tr>
					<?php include_once "include/lm.php" ;?>
					<td valign="top">
					<div class="categories">
					<h1><?=$row1['title'];?> bo'limi</h1>
					<p><?=$row1['text'];?></p>
					
					<?php
						$sql2 = "SELECT * FROM articles WHERE catID = $id";
						$res2 = mysqli_query($db, $sql2);
						
						$articles2 = [];
						while($row2 = mysqli_fetch_assoc($res2)) {
							$articles2[] = $row2;
						}
					
					?>
					
					<?php
						if (mysqli_num_rows($res2)>0):
					
					?>
					
					<?php 
						foreach ($articles2 AS $article2):
					
					?>
					
					
						<div class="card" style="width:100%">
							<img class="card-img-top" src="1.jpg" alt="card  img">
							<div class="card-body">
								<h5 class="card-title"><?=$article2['title']?></h5>
								<p class="card-text"><?=$article2['intro_text']?></p>
								<p>Muallif : Admin | Qo'shildi: <?=$article2['date']?></p>
								<a href="view_articles.php?id=<?=$article2['id']?>" class="btn btn-primary">Batafsil o'qish</a>
						</div>
						</div>
						<hr>
						<?php endforeach;?>
						
						<?php else:?>
						
						<h3>Bu bo'limda maqola mavjud emas!</h3>
						<?php endif?>
					
					</div>
					</td>
					<?php include_once "include/rm.php";?>
				</tr>
			</table>
		</td></tr>
		
		<tr><?php include_once "include/footer.php";?></tr>
	</table>
</body>
</html> 