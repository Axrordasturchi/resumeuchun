<?php
require_once "include/db.php";

$page = str_replace("/","",$_SERVER['SCRIPT_NAME']);
$page = str_replace(".php","",$page);
$sql = "SELECT * FROM pages WHERE page='$page'";
$res = mysqli_query($db, $sql) or die("Sahifani tanlashda xatolik!");
$row = mysqli_fetch_assoc($res);
?>
<!DOCTYPE html>
<html lang="uz-UZ">
<head>
	<meta charset="<?=$row['charset'];?>">
	<title><?=$row['title'];?></title>
	<meta name="Description" content="<?=$row['meta_d'];?>">
	<meta name="Keywords" content="<?=$row['meta_k'];?>">
	<?=$row['htmlcode'];?>
	<link rel="stylesheet" href="bootstrap.css">
	<link rel="stylesheet" href="styleblok.css" >
</head>

<body>
	<table border="1" width="1000" align="center">
		<?php include "include/head.php";?>
		
		<tr><td>
			<table border="1" width="100%" align="center" height="500">
				<tr>
					<?php include_once "include/lm.php" ;?>
					<td valign="top" style="padding:15px;"><h1>Biz bilan aloqa</h1>
					
					<form action="send.php" method="POST" class="form-group" enctype="multipart/form-data">
					
						<label for="fio">F.I.O</label>
						<input name="fio" type="text" class="form-control" id="fio" placeholder="Ismingizni kiriting!" ></br>
						
						<label for="email">E-mail</label>
						<input name="email" type="email" class="form-control" id="email" placeholder="E-mailni kiriting!" ></br>
						
						<label for="subject">Xabar mavzusi</label>
						<input name="subject" type="text" class="form-control" id="subject" placeholder="Xabar mavzusini kiriting!" ></br>
					
						<label for="file">Fayl (.doc, .docx, .pdf)</label><br>
						<input name="file" type="file" id="file"></br>
					
						
						<label for="text">Xabar matni</label>
						<textarea name="text" placeholder="Mazmunini kiriting" class="form-control"></textarea>
						<button type="submit" class="btn btn-primary">Yuborish</button>
						<button type="reset" class="btn btn-danger">Bekor qilish</button>
						
						<h4 style="text-align:center;">Bizning manzil:</h4>
						<hr>
						<address>
						
						
							Jizzax viloyati G'allaorol tumani Buloqboshi QFY Chuvilloq qishlog'i 109-uy
							<a href="tel:+998943441929">+998943441929</a>
						</address>
						<div style="position:relative;overflow:hidden;"><a href="https://yandex.uz/maps?utm_medium=mapframe&utm_source=maps" 
						style="color:#eee;font-size:12px;position:absolute;top:0px;">Яндекс.Карты</a>
						<a href="https://yandex.uz/maps/?bookmarks=true&l=trf%2Ctrfe%2Cmasstransit&ll=67.467880%2C40.044585&mode=bookmarks&rtext=~40.056059%2C67.399850&rtt=auto&ruri=~ymapsbm1%3A%2F%2Fgeo%3Fll%3D67.400%252C40.056%26spn%3D0.829%252C0.447%26text%3DO%25CA%25BBzbekiston%252C%2520Jizzax%2520viloyati%252C%2520G%25CA%25BBallaorol%2520tumani&utm_medium=mapframe&utm_source=maps&z=11.05" style="color:#eee;font-size:12px;position:absolute;top:14px;">
						Яндекс.Карты</a><iframe src="https://yandex.uz/map-widget/v1/-/CCQj6IH3CB" 
						width="560" height="400" frameborder="1" allowfullscreen="true" 
						style="position:relative;"></iframe>
						</div>
						
					</form>
					
					
					</td>
					<?php include_once "include/rm.php";?>
				</tr>
			</table>
		</td></tr>
		
		<tr><?php include_once "include/footer.php";?></tr>
	</table>
</body>
</html> 