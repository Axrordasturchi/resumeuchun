<?php
require_once "include/db.php";

$page = str_replace("/","",$_SERVER['SCRIPT_NAME']);
$page = str_replace(".php","",$page);
$sql = "SELECT * FROM pages WHERE page='$page'";
$res = mysqli_query($db, $sql) or die("Sahifani tanlashda xatolik!");
$row = mysqli_fetch_assoc($res);
?>
<!DOCTYPE html>
<html lang="uz-UZ">
<head>
	<meta charset="<?=$row['charset'];?>">
	<title><?=$row['title'];?></title>
	<meta name="Description" content="<?=$row['meta_d'];?>">
	<meta name="Keywords" content="<?=$row['meta_k'];?>">
	<?=$row['htmlcode'];?>
	<link rel="stylesheet" href="bootstrap.css">
	<link rel="stylesheet" href="styleblok.css" >
</head>

<body>
	<table border="1" width="1000" align="center">
		<?php include "include/head.php";?>
		
		<tr><td>
			<table border="1" width="100%" align="center" height="500">
				<tr>
					<?php include_once "include/lm.php" ;?>
					<td valign="top">
					
					<?php
						$sql = "SELECT * FROM articles ORDER BY date DESC";
						$res = mysqli_query($db, $sql);
						
						$articles = [];
						while($row = mysqli_fetch_assoc($res)) {
							$articles[] = $row;
						}
					?>
					<?php 
						foreach ($articles AS $article):
					
					?>
					
					
						<div class="card" style="width:100%">
							<img class="card-img-top" src="1.jpg" alt="card  img">
							<div class="card-body">
								<h5 class="card-title"><?=$article['title']?></h5>
								<p class="card-text"><?=$article['intro_text']?></p>
								<p>Muallif : Admin | Qo'shildi: <?=$article['date']?></p>
								<a href="view_articles.php?id=<?=$article['id']?>" class="btn btn-primary">Batafsil o'qish</a>
						</div>
						</div>
						<hr>
						<?php endforeach;?>
						
						
					
					</td>
					<?php include_once "include/rm.php";?>
				</tr>
			</table>
		</td></tr>
		
		<tr><?php include_once "include/footer.php";?></tr>
	</table>
</body>
</html> 