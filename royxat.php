
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ro'yxatdan o'tish</title>
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <style type="text/css">
        input{
            margin-bottom: 40px;
            border:none;
            border-bottom:2px solid rgb(13, 150, 241);
        }
       label{
           margin :20px;
       } 
       p{
           font-size:3em;
       }
       form{
           font-size:1.5em;
           margin:0 auto;
       }
       .as{
           margin-top:30px;
       }
       .tepa{
           border-top-left-radius:30px;
           border-top-right-radius:30px;
       }
    </style>
</head>

<body class="bg-primary ">
    <div class="container">
        <div class="as row  " >
            <div class="col-12 text-center bg-warning tepa">
                <P class="font-weight-bold">Ro'yxatdan o'tish</P>
            </div>
            <div class="col-12 bg-white">
                <form class="text-center" action="test.php" method = "post">
                    <label for="ism"> ISM:</label>
                    <input pattern="[a-zA-Z0-9 ]*$" id="ism" name="ism" type="text" placeholder="Ismingizni kiriting"required><br>
                    <label  for="login">LOGIN:</label>
                    <input pattern="[a-zA-Z0-9.,$%@!_^&*()?><{}+ \\\/]*$" id="login" name="login" type="text" required placeholder="Loginni kiriting"><br>
                    <label for="parol">PAROL:</label>
                    <input pattern="[a-zA-Z0-9.,$%@!_^&*()?><{}+ \\\/]*$" id="parol" name="parol" type="password" required placeholder="Parolni kiriting" ><br>
                    <label for="parol2">QAYTA PAROL:</label>
                    <input pattern="[a-zA-Z0-9.,$%@!_^&*()?><{}+ \\/]*$" id="parol2" name="parol2" type="password" required placeholder="Parolni qayta kiriting"><br>
                    <input class="btn btn-info " type="submit" name="submit" value="Ro'yxatdan o'tish">
                </form>
            </div>
        </div>
    </div>
</body>
</html>