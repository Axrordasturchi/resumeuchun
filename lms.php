<?php
   session_start();
?>
<!DOCTYPE html>
<html lang="uzl">
    <head>
        
        <title>LMS</title>
     
        
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap/css/bootstrap.min.css?v=1.3" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/icomoon/style.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/uniform/css/default.css" rel="stylesheet"/>
        <link href="https://lms.tuit.uz/assets/plugins/switchery/switchery.min.css" rel="stylesheet"/>
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet">
        <link rel="stylesheet" href="https://lms.tuit.uz/assets/css/select2.min.css">
    
        
        <!-- Theme Styles -->
        <link href="https://lms.tuit.uz/assets/css/space.css?v=1.5" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/custom.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/fonts.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/style.css?v=1.3" rel="stylesheet">

       
        <style>
        @media (max-width: 768px) { .server-time-t4562h {width: 140px!important; font-size:11px!important; margin-top: -8px!important;} }</style>        
    </head>
    <body class="page-sidebar-fixed page-header-fixed">        
        <!-- Page Container -->
        <div class="page-container">          
            <!-- Page Sidebar -->
            <div class="page-sidebar">              
                <a class="logo-box" href="/">
                    <span>UzMUJF</span>
                    <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
                    <i class="icon-close" id="sidebar-toggle-button-close"></i>
                </a>
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu"> 
                        <li>
                            <a href="https://lms.tuit.uz/dashboard/news">
                                <i class="menu-icon fa fa-dashboard"></i><span>Dashboard</span> 
                            </a>
                        </li>
                        <li class="menu-divider"></li>                           
                                                                                                                                                                                                                                                                                                                                                                        <li class="menu-divider"></li>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <li>
    <div class="page-sidebar__title">Talaba</div>
<li>
    
	<a href="https://lms.tuit.uz/student/subject">
		<i class="menu-icon fa fa-book"></i>
		<span>Fan tanlov</span>
	</a>                              
	<a href="lmsfan.php">
		<i class="menu-icon icon-study"></i>
		<span>Mening fanlarim</span>
	</a>
	<a href="https://lms.tuit.uz/student/schedule">
		<i class="menu-icon fa fa-calendar"></i>
		<span>Dars jadvali</span>
	</a>
	<a href="https://lms.tuit.uz/student/retake">
		<i class="menu-icon fa fa-calculator"></i>
		<span>Qayta o’qish</span>
	</a>
	<a href="https://lms.tuit.uz/student/finals">
		<i class="menu-icon fa fa-question-circle"></i>
		<span>Yakuniy</span>
    </a> 
	<a href="https://lms.tuit.uz/student/study-plan">
		<i class="menu-icon fa fa-file-text-o"></i>
		<span>Individual shaxsiy reja</span>
	</a>
	<a href="https://lms.tuit.uz/student/info">
		<i class="menu-icon fa fa-info-circle"></i>
		<span>Ma'lumot</span>
    </a>
</li>
<li class="menu-divider"></li>  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                        </ul>
                    </div>
                </div>
            </div><!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="https://lms.tuit.uz">
                                        <span>UzMUJF</span>
                                    </a>
                                </div>
                                
                            
                                <ul class="nav navbar-nav navbar-none">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                     <li class="dropdown">
                                        <a href="javascript:void(0)" class="dropdown-toggle dropdown-float" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <img src="https://lms.tuit.uz/assets/images/no-picture.jpg" alt="" class="img-circle">
                                        </a>
                                        <ul class="dropdown-menu dropdown-content">
                                            <li>
                                                <div style="padding:10px 15px 5px">
                                                   <?php 
                                                    echo $_SESSION['name'];?></br>
                                                    <?php
                                                    echo $_SESSION['familiya'];
                                                    ?>
                                                                                                      </div> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="https://lms.tuit.uz/user/profile">Profil sozlamalari</a></li>
                                            <li><a href="https://lms.tuit.uz/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Chiqish</a></li>
                                            <form id="logout-form" action="https://lms.tuit.uz/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="cj5Zpbu1WeOt1r0yzvYci6S238zOxlJhNZ4rjdwq"></form>
                                        </ul>
                                    </li>                            
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->                      
                <!-- Page Inner -->
                <div class="page-inner">
                        
    <div class="row">         
	                <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Yakuniy imtihonlar</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Yakuniy imtihon vaqtlari</p>
                        <p class="panel__date">2020-01-05 12:06:50</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/86" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Yakuniy nazorat</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Yakuniy nazorat ishlarini o’tkazish sanasi o’zgartirildi.</p>
                        <p class="panel__date">2019-12-14 18:54:51</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/85" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Topshiriq muddatlarida o&#039;zgarishlar</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Topshiriq muddatlari kunlari o&#039;zgardi</p>
                        <p class="panel__date">2020-01-05 22:15:19</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/84" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Oraliq nazorat haftasi</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Oraliq nazorati kunlari va yo&#039;riqnomasi</p>
                        <p class="panel__date">2019-11-21 11:15:45</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/82" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Shartnoma to&#039;lovi</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Shartnoma to&#039;lovini amalga oshirmagan 1-bosqich talabalari</p>
                        <p class="panel__date">2019-09-23 18:46:33</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/79" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">1-2 bosqich talaba qizlari</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">&quot;AKT sohasida iqtisod va menejment&quot; fakulteti</p>
                        <p class="panel__date">2019-09-20 10:23:59</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/78" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Kutubxonaga a&#039;zolik</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Kutubxonaga a&#039;zolik hujjatlari</p>
                        <p class="panel__date">2019-09-19 10:49:02</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/77" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Harbiy ro&#039;yhatdan o&#039;tish</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Хarbiy ro&#039;yhatdan o&#039;tish uchun talab etiladigan hujjatlar</p>
                        <p class="panel__date">2019-09-18 12:37:07</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/76" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                    <div class="col-md-4">
                <div class="panel panel-white panel-y panel--height">
                    <div class="panel-heading clearfix">
                        <h4 class="panel__title">Auditoriyalardagi o&#039;zgarishlar</h4>
                    </div>
                    <div class="panel-body">
                        <p class="panel__short-text">Chet tillari fanidan auditoriyalar o&#039;zgardi</p>
                        <p class="panel__date">2019-09-16 23:44:26</p>
                        <a href="https://lms.tuit.uz/dashboard/news/detailed/74" class="btn btn-primary float-right">
                            Batafsil                        </a>
                    </div>
			    </div>
            </div>            
                
    </div>
    <div class="text-right">
        <ul class="pagination" role="navigation">
        
                    <li class="page-item disabled" aria-disabled="true" aria-label="pagination.previous">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>
        
        <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                                                                <li class="page-item"><a class="page-link" href="https://lms.tuit.uz/dashboard/news?page=2">2</a></li>
                                                                                <li class="page-item"><a class="page-link" href="https://lms.tuit.uz/dashboard/news?page=3">3</a></li>
                                                                                <li class="page-item"><a class="page-link" href="https://lms.tuit.uz/dashboard/news?page=4">4</a></li>
                                                        
        
                    <li class="page-item">
                <a class="page-link" href="https://lms.tuit.uz/dashboard/news?page=2" rel="next" aria-label="pagination.next">&rsaquo;</a>
            </li>
            </ul>

    </div> 
                    <div class="page-footer">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="/docs">Dasturchilar uchun</a>
                            </div> 
                            <div class="col-xs-6 text-right">
                                <a href="https://t.me/joinchat/AAAAAE3uzCvQS5qsLRawWw" target="_blank"><i class="fa fa-telegram f-s-20" aria-hidden="true"></i></a>
                            </div>                                                                                       
                        </div>
                        </div>
                    </div>                        
                </div><!-- /Page Inner -->                
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="https://lms.tuit.uz/assets/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/switchery/switchery.min.js"></script>
        <script src="https://lms.tuit.uz/assets/js/space.min.js"></script>
        <script src="https://lms.tuit.uz/assets/js/lms.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="https://lms.tuit.uz/assets/js/pages/form-elements.js"></script>
        <script src="https://lms.tuit.uz/assets/js/select2.full.min.js"></script>

    </body>
</html>