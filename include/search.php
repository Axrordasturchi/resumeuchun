<?php
require_once "include/db.php";

if(isset($_GET['q']) AND !empty($_GET['q'])) {
	$q = $_GET['q'];
}
else {
	header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="uz-UZ">
<head>
	<meta charset="UTF-8">
	<title><?=$q?> qidiruv so'zi | blog.uz</title>
	<meta name="Description" content="<?=$q?> qidiruv so'zi | blog.uz">
	<meta name="Keywords" content="<?=$q?> qidiruv so'zi | blog.uz">

	<link rel="stylesheet" href="bootstrap.css">
	<link rel="stylesheet" href="styleblok.css">
</head>

<body>
	<table border="1" width="1000" align="center">
		<?php include "include/head.php";?>
		
		<tr><td>
			<table border="1" width="100%" align="center" height="500">
				<tr>
					<?php include_once "include/lm.php";?>
					<td valign="top">
					
					<?php
						$sql = "SELECT articles.title AS title, articles.intro_text AS intro_text,articles.date 
						AS date,articles.id AS id, category.title AS cattitle, category.id AS idcat FROM articles INNER JOIN  
						category.id = articles.catID WHERE 'full_text' LIKE '%$q%' ORDER BY date DESC";
						
						$res = mysqli_query($db, $sql);
						$articles = [];
						while($row = mysqli_fetch_assoc($res)) {
							$articles[] = $row;
						}
					
					?>
					<?php 
						foreach ($articles AS $article):
					
					?>
					
					
						<div class="card" style="width:100%">
							<img class="card-img-top" src="1.jpg" alt="card  img">
							<div class="card-body">
								<h5 class="card-title"><?=$article['title']?></h5>
								<p class="card-text"><?=$article['intro_text']?></p>
								<p>Muallif : Admin | Qo'shildi: <?=$article['date']?></p>
								<a href="view_articles.php?id=<?=$article['id']?>" class="btn btn-primary">Batafsil o'qish</a>
						</div>
						</div>
						<hr>
						<?php endforeach;?>
						
						
					
					</td>
					<?php include_once "include/rm.php";?>
				</tr>
			</table>
		</td></tr>
		
		<tr><?php include_once "include/footer.php";?></tr>
	</table>
</body>
</html> 