<!DOCTYPE html>
<html lang="uzl">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="LMS">
        <meta name="keywords" content="admin,TUIT">
        <meta name="author" content="stacks">
        <meta name="csrf-token" content="XlBZFas8no4IfqFZZLuSto8mvWQZtPjH4vvv2mov">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->    
        <!-- Title -->
        <title>LMS</title>
        <!-- Styles -->
        
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap/css/bootstrap.min.css?v=1.3" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/icomoon/style.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/uniform/css/default.css" rel="stylesheet"/>
        <link href="https://lms.tuit.uz/assets/plugins/switchery/switchery.min.css" rel="stylesheet"/>
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet">
        <link rel="stylesheet" href="https://lms.tuit.uz/assets/css/select2.min.css">
    
        <link href="https://lms.tuit.uz/assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" />	
<link href="https://lms.tuit.uz/assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" />	

        <!-- Theme Styles -->
        <link href="https://lms.tuit.uz/assets/css/space.css?v=1.5" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/custom.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/fonts.css" rel="stylesheet">
        <link href="https://lms.tuit.uz/assets/css/style.css?v=1.3" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
        @media (max-width: 768px) { .server-time-t4562h {width: 140px!important; font-size:11px!important; margin-top: -8px!important;} }</style>        
    </head>
    <body class="page-sidebar-fixed page-header-fixed">        
        <!-- Page Container -->
        <div class="page-container">          
            <!-- Page Sidebar -->
            <div class="page-sidebar">              
                <a class="logo-box" href="/">
                    <span>UzMUJF</span>
                    <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
                    <i class="icon-close" id="sidebar-toggle-button-close"></i>
                </a>
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu"> 
                        <li>
                            <a href="https://lms.tuit.uz/dashboard/news">
                                <i class="menu-icon fa fa-dashboard"></i><span>Dashboard</span> 
                            </a>
                        </li>
                        <li class="menu-divider"></li>                           
                                                                                                                                                                                                                                                                                                                                                                        <li class="menu-divider"></li>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <li>
    <div class="page-sidebar__title">Talaba</div>
<li>
    
	<a href="https://lms.tuit.uz/student/subject">
		<i class="menu-icon fa fa-book"></i>
		<span>Fan tanlov</span>
	</a>                              
	<a href="https://lms.tuit.uz/student/my-courses">
		<i class="menu-icon icon-study"></i>
		<span>Mening fanlarim</span>
	</a>
	<a href="https://lms.tuit.uz/student/schedule">
		<i class="menu-icon fa fa-calendar"></i>
		<span>Dars jadvali</span>
	</a>
	<a href="https://lms.tuit.uz/student/retake">
		<i class="menu-icon fa fa-calculator"></i>
		<span>Qayta o’qish</span>
	</a>
	<a href="https://lms.tuit.uz/student/finals">
		<i class="menu-icon fa fa-question-circle"></i>
		<span>Yakuniy</span>
    </a> 
	<a href="https://lms.tuit.uz/student/study-plan">
		<i class="menu-icon fa fa-file-text-o"></i>
		<span>Individual shaxsiy reja</span>
	</a>
	<a href="https://lms.tuit.uz/student/info">
		<i class="menu-icon fa fa-info-circle"></i>
		<span>Ma'lumot</span>
    </a>
</li>
<li class="menu-divider"></li>  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                        </ul>
                    </div>
                </div>
            </div><!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="https://lms.tuit.uz">
                                        <span>LMS PRO</span>
                                    </a>
                                </div>
                                
                            
                                <ul class="nav navbar-nav navbar-none">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    
                                    
                                          
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" class="dropdown-toggle dropdown-float" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <img src="https://lms.tuit.uz/assets/images/no-picture.jpg" alt="" class="img-circle">
                                        </a>
                                        <ul class="dropdown-menu dropdown-content">
                                            <li>
                                                <div style="padding:10px 15px 5px">
                                                    Abdusamatov Shohzod
                                                </div> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="https://lms.tuit.uz/user/profile">Profil sozlamalari</a></li>
                                            <li><a href="https://lms.tuit.uz/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Chiqish</a></li>
                                            <form id="logout-form" action="https://lms.tuit.uz/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="XlBZFas8no4IfqFZZLuSto8mvWQZtPjH4vvv2mov"></form>
                                        </ul>
                                    </li>                            
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->                      
                <!-- Page Inner -->
                <div class="page-inner">
                    
<div class="row">
  <div class="col-sm-6">
    <div class="page-title">
      <h3 class="breadcrumb-header">Mening fanlarim</h3>
    </div>
  </div>
  <div class="col-sm-6 text-right">
      <form class="form-inline m-b-sm js-filter-form m-t-md">
          <div class="form-group">
              <select name="semester_id" class="form-control js-semester">
                                    <option value="7" selected>
                      2019-2020
                      Ikkinchi semester                               
                  </option>
                                    <option value="6" >
                      2019-2020
                      Birinchi semester uchun qayta o&#039;qish                               
                  </option>
                                    <option value="5" >
                      2019-2020
                      Birinchi semester                               
                  </option>
                                    <option value="4" >
                      2018-2019
                      Ikkinchi semester uchun qayta o&#039;qish                               
                  </option>
                                    <option value="3" >
                      2018-2019
                      Ikkinchi semester                               
                  </option>
                                    <option value="2" >
                      2018-2019
                      Birinchi semester uchun qayta o&#039;qish                               
                  </option>
                                    <option value="1" >
                      2018-2019
                      Birinchi semester                               
                  </option>
                                </select>
          </div>
      </form>
  </div>
</div>
<div class="table-responsive">
  <table class="table" id="datatable">
    <thead>
      <tr>
        <th scope="col" width="250">Fan</th>
        <th scope="col">O’qituvchi</th>
        <th scope="col">Davomat</th>
        <th style="width: 100px;">Amal</th>
        <th>Reja</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
</div>
                    <div class="page-footer">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="/docs">Dasturchilar uchun</a>
                            </div> 
                            <div class="col-xs-6 text-right">
                                <a href="https://t.me/joinchat/AAAAAE3uzCvQS5qsLRawWw" target="_blank"><i class="fa fa-telegram f-s-20" aria-hidden="true"></i></a>
                            </div>                                                                                       
                        </div>
                        </div>
                    </div>                        
                </div><!-- /Page Inner -->                
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="https://lms.tuit.uz/assets/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/switchery/switchery.min.js"></script>
        <script src="https://lms.tuit.uz/assets/js/space.min.js"></script>
        <script src="https://lms.tuit.uz/assets/js/lms.js"></script>
        <script src="https://lms.tuit.uz/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="https://lms.tuit.uz/assets/js/pages/form-elements.js"></script>
        <script src="https://lms.tuit.uz/assets/js/select2.full.min.js"></script>

        

        <script src="https://lms.tuit.uz/assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script src="https://lms.tuit.uz/assets/js/crud.js"></script>
<script>
    var crud = new Crud({

        list: {
            url: "https://lms.tuit.uz/student/my-courses/data",

            data: {
              semester_id: $('.js-semester :selected').val(),                
            },

            datatable: {
                columns: [
                    {data: 'subject', name: 'subject'},
                    {data: 'teachers', name: 'teachers'},
                    {data: 'attendance', name: 'attendance'}
                ],
                columnDefs: [
                  {
                    targets: 1,
                    data: null,
                    searchable:false, 
                    render: function (row, type, val, meta) {
                      var _html = '';
                      var _teachers = val.teachers.split("###");
                      var _streams = val.streams.split("###");
                      $.each(val.teachers.split("###"), function(i, teacher) {
                        _html += '<span class="text-primary">'
                              + _streams[i] + '</span> - '
                              + teacher
                              + '<br/>';
                      });
                      return _html;
                    }
                  },
                    {
                      targets: 2,
                      data: null,
                      searchable:false, 
                      render: function (row, type, val, meta) {
                        return '<a href="/student/attendance/?subject_id=' + val.id + '&semester_id=' + val.semester_id + '" class="btn ' + (val.attendance >= 5 ? 'btn-danger' : 'btn-default') + '">'
                          + val.attendance
                          + '</a>';
                      }
                    },
                    {
                        targets: 3,
                        data: null,
                        searchable:false, 
                        render: function (row, type, val, meta) {
                          return '<a href="/student/my-courses/show/' + val.id + '" class="btn btn-default">'
                            + '<i class="fa fa-clone"></i> Vazifalar'
                            + '</a> ';
                        }
                    },
                    {
                        targets: 4,
                        data: null,
                        searchable:false, 
                        render: function (row, type, val, meta) {
                          return '<a href="/student/calendar/' + val.id + '" class="btn btn-default">'
                            + '<i class="fa fa-calendar"></i>'
                            + '</a> ';
                        }
                    }
                ]
            }
        }        
    });
    
    $('.js-semester').on('change', function(e) {
        crud.options.list.data.semester_id = $(this).val()
        crud.datatable.draw();
    }); 
</script>
    </body>
</html>